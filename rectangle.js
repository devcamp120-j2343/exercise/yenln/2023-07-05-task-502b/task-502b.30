class Rectangle {
    constructor(a, b) {
        this.width = a;
        this.length = b;
    }
    getArea() {
        return this.width * this.length;
    }
}

export default Rectangle;