import Rectangle from "./rectangle.js";
import Square from "./Square.js";
let rec1 = new Rectangle(20, 5);
console.log(`width: ${rec1.width}`);
console.log(`length: ${rec1.length}`);
console.log(`getArea: ${rec1.getArea()}`);
let rec2 = new Rectangle(10, 25);
console.log(`width: ${rec2.width}`);
console.log(`length: ${rec2.length}`);
console.log(`getArea: ${rec2.getArea()}`);


let square1 = new Square(24,15,20);
console.log(`width: ${square1.width}`);
console.log(`length: ${square1.length}`);
console.log(`getArea: ${square1.getArea()}`);
let square2 = new Square(19);
console.log(`width: ${square1.width}`);
console.log(`length: ${square1.length}`);
console.log(`length: ${square1.perimeter}`);
console.log(`getArea: ${square1.getArea()}`);
console.log(`getArea: ${square1.getPerimeter()}`);

console.log(rec1 instanceof Rectangle);
console.log(square1 instanceof Rectangle);
console.log(square2 instanceof Rectangle);

console.log(rec1 instanceof Square);
console.log(square1 instanceof Square);
console.log(square2 instanceof Square);