import Rectangle from "./rectangle.js";
class Square extends Rectangle {
    constructor(a, b) {
        super(a, b);
        //this.width = width;
        //this.length = length;
        this.perimeter = a * 4;
    }

    getPerimeter() {
        return this.perimeter;
    }
}

export default Square;